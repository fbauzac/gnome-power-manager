# Hungarian translation for gnome-power-manager.
# Copyright (C) 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2013, 2014, 2015, 2016, 2017. Free Software Foundation, Inc.
# This file is distributed under the same license as the gnome-power-manager package.
#
# Gabor Kelemen <kelemeng at gnome dot hu>, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2016.
# Szabolcs Varga <shirokuma at shirokuma dot hu>, 2006.
# Kováts Dóra <dorcssa at gmail dot com>, 2008.
# Balázs Úr <urbalazs at gmail dot com>, 2013, 2014, 2015, 2016, 2017.
msgid ""
msgstr ""
"Project-Id-Version: gnome-power-manager master\n"
"Report-Msgid-Bugs-To: https://bugzilla.gnome.org/enter_bug.cgi?product=gnome-"
"power-manager&keywords=I18N+L10N&component=general\n"
"POT-Creation-Date: 2017-02-23 15:33+0000\n"
"PO-Revision-Date: 2017-03-15 17:48+0100\n"
"Last-Translator: Balázs Úr <urbalazs@gmail.com>\n"
"Language-Team: Hungarian <openscope at googlegroups dot com>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.2\n"
"Plural-Forms:  nplurals=2; plural=(n != 1);\n"

#: data/appdata/org.gnome.PowerStats.appdata.xml.in:7
msgid "GNOME Power Statistics"
msgstr "GNOME energiastatisztika"

#: data/appdata/org.gnome.PowerStats.appdata.xml.in:8
#: data/org.gnome.PowerStats.desktop.in.in:4
msgid "Observe power management"
msgstr "Energiagazdálkodás megfigyelése"

#: data/appdata/org.gnome.PowerStats.appdata.xml.in:10
msgid ""
"Power Statistics can show historical and current battery information and "
"programs waking up that use power."
msgstr ""
"Az energiastatisztika képes megjeleníteni az előzményeket és a jelenlegi "
"akkumulátor információkat, valamint az ébredő programokat, amelyek energiát "
"használnak."

#: data/appdata/org.gnome.PowerStats.appdata.xml.in:14
msgid ""
"You probably only need to install this application if you are having "
"problems with your laptop battery, or are trying to work out what programs "
"are using significant amounts of power."
msgstr ""
"Valószínűleg csak ennek az alkalmazásnak a telepítésére van szüksége, ha "
"problémái vannak a laptop akkumulátorával, vagy hogy megpróbálja kideríteni, "
"mely programok használnak jelentős mennyiségű energiát."

#: data/org.gnome.power-manager.gschema.xml:5
msgid "Whether we should show the history data points"
msgstr "Megjelenjenek-e az előzmények adatpontjai"

#: data/org.gnome.power-manager.gschema.xml:6
msgid ""
"Whether we should show the history data points in the statistics window."
msgstr "Megjelenjenek-e az előzmények adatpontjai a statisztikaablakban?"

#: data/org.gnome.power-manager.gschema.xml:10
msgid "Whether we should smooth the history data"
msgstr "Az előzményadatok simításra kerüljenek-e"

#: data/org.gnome.power-manager.gschema.xml:11
msgid "Whether we should smooth the history data in the graph."
msgstr "Az előzményadatok simításra kerüljenek-e a grafikonon?"

#: data/org.gnome.power-manager.gschema.xml:15
msgid "The default graph type to show for history"
msgstr "Az előzményekhez megjelenítendő alapértelmezett grafikontípus"

#: data/org.gnome.power-manager.gschema.xml:16
msgid "The default graph type to show in the history window."
msgstr "Az előzményablakban megjelenítendő alapértelmezett grafikontípus."

#: data/org.gnome.power-manager.gschema.xml:20
msgid "The maximum time displayed for history"
msgstr "Az előzményekhez megjelenítendő maximális időtartam"

#: data/org.gnome.power-manager.gschema.xml:21
msgid ""
"The maximum duration of time displayed on the x-axis of the history graph."
msgstr "Az előzménygrafikon X tengelyén megjelenítendő maximális időtartam."

#: data/org.gnome.power-manager.gschema.xml:25
msgid "Whether we should show the stats data points"
msgstr "Megjelenjenek-e a statisztika adatpontjai"

#: data/org.gnome.power-manager.gschema.xml:26
msgid "Whether we should show the stats data points in the statistics window."
msgstr "Megjelenjenek-e a statisztika adatpontjai a statisztikaablakban?"

#: data/org.gnome.power-manager.gschema.xml:30
msgid "Whether we should smooth the stats data"
msgstr "A statisztikaadatok simításra kerüljenek-e"

#: data/org.gnome.power-manager.gschema.xml:31
msgid "Whether we should smooth the stats data in the graph."
msgstr "A statisztikaadatok simításra kerüljenek-e a grafikonon?"

#: data/org.gnome.power-manager.gschema.xml:35
msgid "The default graph type to show for stats"
msgstr "A statisztikákhoz megjelenítendő alapértelmezett grafikontípus"

#: data/org.gnome.power-manager.gschema.xml:36
msgid "The default graph type to show in the stats window."
msgstr "A statisztikaablakban megjelenítendő alapértelmezett grafikontípus."

#: data/org.gnome.power-manager.gschema.xml:40
msgid "The index of the page number to show by default"
msgstr "Az alapértelmezésben megjelenítendő oldalszám indexe"

#: data/org.gnome.power-manager.gschema.xml:41
msgid ""
"The index of the page number to show by default which is used to return "
"focus to the correct page."
msgstr ""
"Az alapértelmezésben megjelenítendő oldalszám indexe, amely a fókusz "
"megfelelő oldalra való visszaadására használatos."

#: data/org.gnome.power-manager.gschema.xml:45
msgid "The ID of the last device selected"
msgstr "Az utoljára kiválasztott eszköz azonosítója"

#: data/org.gnome.power-manager.gschema.xml:46
msgid ""
"The identifier of the last device which is used to return focus to the "
"correct device."
msgstr ""
"Az utolsó eszköz azonosítója, amely a fókusz megfelelő eszköznek való "
"visszaadására használatos."

#. TRANSLATORS: shown on the titlebar
#. TRANSLATORS: the program name
#: data/org.gnome.PowerStats.desktop.in.in:3 src/gpm-statistics.c:1275
#: src/gpm-statistics.c:1658 src/gpm-statistics.ui:8
msgid "Power Statistics"
msgstr "Energiastatisztika"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.PowerStats.desktop.in.in:6
msgid "battery;consumption;charge;"
msgstr "akkumulátor;fogyasztás;töltés;"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: data/org.gnome.PowerStats.desktop.in.in:8
msgid "org.gnome.PowerStats"
msgstr "org.gnome.PowerStats"

#. Translators: This is %i days
#: src/egg-graph-widget.c:393
#, c-format
msgid "%id"
msgstr "%in"

#. Translators: This is %i days %02i hours
#: src/egg-graph-widget.c:396
#, c-format
msgid "%id%02ih"
msgstr "%dn %02ió"

#. Translators: This is %i hours
#: src/egg-graph-widget.c:401
#, c-format
msgid "%ih"
msgstr "%ió"

#. Translators: This is %i hours %02i minutes
#: src/egg-graph-widget.c:404
#, c-format
msgid "%ih%02im"
msgstr "%ió %02im"

#. Translators: This is %2i minutes
#: src/egg-graph-widget.c:409
#, c-format
msgid "%2im"
msgstr "%2ip"

#. Translators: This is %2i minutes %02i seconds
#: src/egg-graph-widget.c:412
#, c-format
msgid "%2im%02i"
msgstr "%2ip %02i"

#. TRANSLATORS: This is ms
#: src/egg-graph-widget.c:416
#, c-format
msgid "%.0fms"
msgstr "%.0fms"

#. Translators: This is %2i seconds
#: src/egg-graph-widget.c:419
#, c-format
msgid "%2is"
msgstr "%2imp"

#. TRANSLATORS: This is %i Percentage
#: src/egg-graph-widget.c:423
#, c-format
msgid "%i%%"
msgstr "%i%%"

#. TRANSLATORS: This is %.1f Watts
#: src/egg-graph-widget.c:426
#, c-format
msgid "%.1fW"
msgstr "%.1fW"

#. TRANSLATORS: This is %.1f Volts
#: src/egg-graph-widget.c:431
#, c-format
msgid "%.1fV"
msgstr "%.1fV"

#. TRANSLATORS: This is %.1f nanometers
#: src/egg-graph-widget.c:434
#, c-format
msgid "%.0f nm"
msgstr "%.0f nm"

#. TRANSLATORS: the rate of discharge for the device
#: src/gpm-statistics.c:82 src/gpm-statistics.c:680
msgid "Rate"
msgstr "Sebesség"

#: src/gpm-statistics.c:83
msgid "Charge"
msgstr "Töltés"

#: src/gpm-statistics.c:84 src/gpm-statistics.c:694
msgid "Time to full"
msgstr "Idő a feltöltésig"

#: src/gpm-statistics.c:85 src/gpm-statistics.c:699
msgid "Time to empty"
msgstr "Idő a lemerülésig"

#: src/gpm-statistics.c:92
msgid "10 minutes"
msgstr "10 perc"

#: src/gpm-statistics.c:93
msgid "2 hours"
msgstr "2 óra"

#: src/gpm-statistics.c:94
msgid "6 hours"
msgstr "6 óra"

#: src/gpm-statistics.c:95
msgid "1 day"
msgstr "1 nap"

#: src/gpm-statistics.c:96
msgid "1 week"
msgstr "1 hét"

#. TRANSLATORS: what we've observed about the device
#: src/gpm-statistics.c:105
msgid "Charge profile"
msgstr "Töltés profilja"

# fixme
#: src/gpm-statistics.c:106
msgid "Discharge profile"
msgstr "Kisülés profilja"

#. TRANSLATORS: how accurately we can predict the time remaining of the battery
#: src/gpm-statistics.c:108
msgid "Charge accuracy"
msgstr "Töltés pontossága"

# fixme
#: src/gpm-statistics.c:109
msgid "Discharge accuracy"
msgstr "Kisülés pontossága"

#. TRANSLATORS: system power cord
#: src/gpm-statistics.c:239
msgid "AC adapter"
msgid_plural "AC adapters"
msgstr[0] "Hálózati csatlakozó"
msgstr[1] "Hálózati csatlakozók"

#. TRANSLATORS: laptop primary battery
#: src/gpm-statistics.c:243
msgid "Laptop battery"
msgid_plural "Laptop batteries"
msgstr[0] "Noteszgép-akkumulátor"
msgstr[1] "Noteszgép-akkumulátorok"

#. TRANSLATORS: battery-backed AC power source
#: src/gpm-statistics.c:247
msgid "UPS"
msgid_plural "UPSs"
msgstr[0] "Szünetmentes táp"
msgstr[1] "Szünetmentes táp"

#. TRANSLATORS: a monitor is a device to measure voltage and current
#: src/gpm-statistics.c:251
msgid "Monitor"
msgid_plural "Monitors"
msgstr[0] "Monitor"
msgstr[1] "Monitorok"

#. TRANSLATORS: wireless mice with internal batteries
#: src/gpm-statistics.c:255
msgid "Mouse"
msgid_plural "Mice"
msgstr[0] "Egér"
msgstr[1] "Egér"

#. TRANSLATORS: wireless keyboard with internal battery
#: src/gpm-statistics.c:259
msgid "Keyboard"
msgid_plural "Keyboards"
msgstr[0] "Billentyűzet"
msgstr[1] "Billentyűzet"

#. TRANSLATORS: portable device
#: src/gpm-statistics.c:263
msgid "PDA"
msgid_plural "PDAs"
msgstr[0] "PDA"
msgstr[1] "PDA-k"

#. TRANSLATORS: cell phone (mobile...)
#: src/gpm-statistics.c:267
msgid "Cell phone"
msgid_plural "Cell phones"
msgstr[0] "Mobiltelefon"
msgstr[1] "Mobiltelefonok"

#. TRANSLATORS: media player, mp3 etc
#: src/gpm-statistics.c:272
msgid "Media player"
msgid_plural "Media players"
msgstr[0] "Médialejátszó"
msgstr[1] "Médialejátszó"

#. TRANSLATORS: tablet device
#: src/gpm-statistics.c:276
msgid "Tablet"
msgid_plural "Tablets"
msgstr[0] "Táblagép"
msgstr[1] "Táblagép"

#. TRANSLATORS: tablet device
#: src/gpm-statistics.c:280
msgid "Computer"
msgid_plural "Computers"
msgstr[0] "Számítógép"
msgstr[1] "Számítógép"

#. TRANSLATORS: battery technology
#: src/gpm-statistics.c:297
msgid "Lithium Ion"
msgstr "Lítiumion"

#. TRANSLATORS: battery technology
#: src/gpm-statistics.c:301
msgid "Lithium Polymer"
msgstr "Lítiumpolimer"

#. TRANSLATORS: battery technology
#: src/gpm-statistics.c:305
msgid "Lithium Iron Phosphate"
msgstr "Lítium-vasfoszfát"

#. TRANSLATORS: battery technology
#: src/gpm-statistics.c:309
msgid "Lead acid"
msgstr "Ólmos-savas"

#. TRANSLATORS: battery technology
#: src/gpm-statistics.c:313
msgid "Nickel Cadmium"
msgstr "Nikkel-kadmium"

#. TRANSLATORS: battery technology
#: src/gpm-statistics.c:317
msgid "Nickel metal hydride"
msgstr "Nikkel-fém hidrid"

#. TRANSLATORS: battery technology
#: src/gpm-statistics.c:321
msgid "Unknown technology"
msgstr "Ismeretlen technológia"

#. TRANSLATORS: battery state
#: src/gpm-statistics.c:338
msgid "Charging"
msgstr "Töltés"

#. TRANSLATORS: battery state
#: src/gpm-statistics.c:342
msgid "Discharging"
msgstr "Kisülés"

#. TRANSLATORS: battery state
#: src/gpm-statistics.c:346
msgid "Empty"
msgstr "Lemerült"

#. TRANSLATORS: battery state
#: src/gpm-statistics.c:350
msgid "Charged"
msgstr "Feltöltve"

#. TRANSLATORS: battery state
#: src/gpm-statistics.c:354
msgid "Waiting to charge"
msgstr "Várakozás töltésre"

# fixme
#. TRANSLATORS: battery state
#: src/gpm-statistics.c:358
msgid "Waiting to discharge"
msgstr "Várakozás kisülésre"

#. TRANSLATORS: battery state
#. TRANSLATORS: this is when the stats time is not known
#: src/gpm-statistics.c:362 src/gpm-statistics.c:518
msgid "Unknown"
msgstr "Ismeretlen"

#: src/gpm-statistics.c:379
msgid "Attribute"
msgstr "Attribútum"

#: src/gpm-statistics.c:386
msgid "Value"
msgstr "Érték"

#: src/gpm-statistics.c:400
msgid "Image"
msgstr "Kép"

#: src/gpm-statistics.c:406
msgid "Description"
msgstr "Leírás"

#: src/gpm-statistics.c:422 src/gpm-statistics.c:619
msgid "Type"
msgstr "Típus"

#: src/gpm-statistics.c:428
msgid "ID"
msgstr "Azonosító"

#: src/gpm-statistics.c:435 src/gpm-statistics.ui:516
msgid "Wakeups"
msgstr "Ébredések"

#: src/gpm-statistics.c:442
msgid "Command"
msgstr "Parancs"

#: src/gpm-statistics.c:449 src/gpm-statistics.ui:66
msgid "Details"
msgstr "Részletek"

#. TRANSLATORS: this is a time value, usually to show on a graph
#: src/gpm-statistics.c:522
#, c-format
msgid "%.0f second"
msgid_plural "%.0f seconds"
msgstr[0] "%.0f másodperc"
msgstr[1] "%.0f másodperc"

#. TRANSLATORS: this is a time value, usually to show on a graph
#: src/gpm-statistics.c:527
#, c-format
msgid "%.1f minute"
msgid_plural "%.1f minutes"
msgstr[0] "%.1f perc"
msgstr[1] "%.1f perc"

#. TRANSLATORS: this is a time value, usually to show on a graph
#: src/gpm-statistics.c:532
#, c-format
msgid "%.1f hour"
msgid_plural "%.1f hours"
msgstr[0] "%.1f óra"
msgstr[1] "%.1f óra"

#. TRANSLATORS: this is a time value, usually to show on a graph
#: src/gpm-statistics.c:536
#, c-format
msgid "%.1f day"
msgid_plural "%.1f days"
msgstr[0] "%.1f nap"
msgstr[1] "%.1f nap"

#: src/gpm-statistics.c:542
msgid "Yes"
msgstr "Igen"

#: src/gpm-statistics.c:542
msgid "No"
msgstr "Nem"

#. TRANSLATORS: the device ID of the current device, e.g. "battery0"
#: src/gpm-statistics.c:617
msgid "Device"
msgstr "Eszköz"

#: src/gpm-statistics.c:621
msgid "Vendor"
msgstr "Gyártó"

#: src/gpm-statistics.c:623
msgid "Model"
msgstr "Modell"

#: src/gpm-statistics.c:625
msgid "Serial number"
msgstr "Sorozatszám"

#. TRANSLATORS: a boolean attribute that means if the device is supplying the
#. * main power for the computer. For instance, an AC adapter or laptop battery
#. * would be TRUE,  but a mobile phone or mouse taking power is FALSE
#: src/gpm-statistics.c:630
msgid "Supply"
msgstr "Ellátás"

#: src/gpm-statistics.c:633
#, c-format
msgid "%u second"
msgid_plural "%u seconds"
msgstr[0] "%u másodperc"
msgstr[1] "%u másodperc"

#. TRANSLATORS: when the device was last updated with new data. It's
#. * usually a few seconds when a device is discharging or charging.
#: src/gpm-statistics.c:637
msgid "Refreshed"
msgstr "Frissítve"

#. TRANSLATORS: Present is whether the device is currently attached
#. * to the computer, as some devices (e.g. laptop batteries) can
#. * be removed, but still observed as devices on the system
#: src/gpm-statistics.c:647
msgid "Present"
msgstr "Jelen van"

#. TRANSLATORS: If the device can be recharged, e.g. lithium
#. * batteries rather than alkaline ones
#: src/gpm-statistics.c:654
msgid "Rechargeable"
msgstr "Újratölthető"

#. TRANSLATORS: The state of the device, e.g. "Changing" or "Fully charged"
#: src/gpm-statistics.c:660
msgid "State"
msgstr "Állapot"

#: src/gpm-statistics.c:664
msgid "Energy"
msgstr "Energia"

#: src/gpm-statistics.c:667
msgid "Energy when empty"
msgstr "Energia lemerülve"

#: src/gpm-statistics.c:670
msgid "Energy when full"
msgstr "Energia feltöltve"

#: src/gpm-statistics.c:673
msgid "Energy (design)"
msgstr "Energia (tervezett)"

#: src/gpm-statistics.c:687
msgid "Voltage"
msgstr "Feszültség"

#. TRANSLATORS: the amount of charge the cell contains
#: src/gpm-statistics.c:709
msgid "Percentage"
msgstr "Százalék"

#. TRANSLATORS: the capacity of the device, which is basically a measure
#. * of how full it can get, relative to the design capacity
#: src/gpm-statistics.c:716
msgid "Capacity"
msgstr "Kapacitás"

#. TRANSLATORS: the type of battery, e.g. lithium or nikel metal hydroxide
#: src/gpm-statistics.c:721
msgid "Technology"
msgstr "Technológia"

#. TRANSLATORS: this is when the device is plugged in, typically
#. * only shown for the ac adaptor device
#: src/gpm-statistics.c:726
msgid "Online"
msgstr "Online"

#. TRANSLATORS: the command line was not provided
#: src/gpm-statistics.c:1025
msgid "No data"
msgstr "Nincs adat"

#. TRANSLATORS: kernel module, usually a device driver
#: src/gpm-statistics.c:1032 src/gpm-statistics.c:1037
msgid "Kernel module"
msgstr "Rendszermagmodul"

#. TRANSLATORS: kernel housekeeping
#: src/gpm-statistics.c:1042
msgid "Kernel core"
msgstr "Rendszermag"

#. TRANSLATORS: interrupt between processors
#: src/gpm-statistics.c:1047
msgid "Interprocessor interrupt"
msgstr "Processzorközi megszakítás"

#. TRANSLATORS: unknown interrupt
#: src/gpm-statistics.c:1052
msgid "Interrupt"
msgstr "Megszakítás"

#. TRANSLATORS: the keyboard and mouse device event
#: src/gpm-statistics.c:1095
msgid "PS/2 keyboard/mouse/touchpad"
msgstr "PS/2 billentyűzet/egér/érintőtábla"

#. TRANSLATORS: ACPI, the Intel power standard on laptops and desktops
#: src/gpm-statistics.c:1098
msgid "ACPI"
msgstr "ACPI"

#. TRANSLATORS: serial ATA is a new style of hard disk interface
#: src/gpm-statistics.c:1101
msgid "Serial ATA"
msgstr "Soros ATA"

#. TRANSLATORS: this is the old-style ATA interface
#: src/gpm-statistics.c:1104
msgid "ATA host controller"
msgstr "ATA gépvezérlő"

#. TRANSLATORS: 802.11 wireless adaptor
#: src/gpm-statistics.c:1107
msgid "Intel wireless adaptor"
msgstr "Intel vezeték nélküli csatoló"

#. TRANSLATORS: a timer is something that fires periodically.
#. * The parameter is a process name, e.g. "firefox-bin".
#. * This is shown when the timer wakes up.
#: src/gpm-statistics.c:1114 src/gpm-statistics.c:1119
#: src/gpm-statistics.c:1124 src/gpm-statistics.c:1129
#: src/gpm-statistics.c:1134
#, c-format
msgid "Timer %s"
msgstr "Időzítő: %s"

#. TRANSLATORS: the parameter is the name of task that's woken up from sleeping.
#. * This is shown when the task wakes up.
#: src/gpm-statistics.c:1138
#, c-format
msgid "Sleep %s"
msgstr "Alvó: %s"

#. TRANSLATORS: this is the name of a new realtime task.
#: src/gpm-statistics.c:1141
#, c-format
msgid "New task %s"
msgstr "Új feladat: %s"

#. TRANSLATORS: this is the name of a task that's woken to check state.
#. * This is shown when the task wakes up.
#: src/gpm-statistics.c:1145
#, c-format
msgid "Wait %s"
msgstr "Várakozik: %s"

#. TRANSLATORS: this is the name of a work queue.
#. * A work queue is a list of work that has to be done.
#: src/gpm-statistics.c:1149 src/gpm-statistics.c:1153
#, c-format
msgid "Work queue %s"
msgstr "Munkasor: %s"

#. TRANSLATORS: this is when the networking subsystem clears out old entries
#: src/gpm-statistics.c:1156
#, c-format
msgid "Network route flush %s"
msgstr "Hálózati útválasztás ürítése: %s"

#. TRANSLATORS: this is the name of an activity on the USB bus
#: src/gpm-statistics.c:1159
#, c-format
msgid "USB activity %s"
msgstr "USB tevékenység: %s"

#. TRANSLATORS: we've timed out of an aligned timer, with the name
#: src/gpm-statistics.c:1162
#, c-format
msgid "Wakeup %s"
msgstr "Ébredések: %s"

#. TRANSLATORS: interupts on the system required for basic operation
#: src/gpm-statistics.c:1165
msgid "Local interrupts"
msgstr "Helyi megszakítások"

#. TRANSLATORS: interrupts when a task gets moved from one core to another
#: src/gpm-statistics.c:1168
msgid "Rescheduling interrupts"
msgstr "Átütemezett megszakítások"

#. TRANSLATORS: shown on the titlebar
#: src/gpm-statistics.c:1265
msgid "Device Information"
msgstr "Eszközinformációk"

#. TRANSLATORS: shown on the titlebar
#: src/gpm-statistics.c:1267
msgid "Device History"
msgstr "Eszköz előzményei"

# fixme
#. TRANSLATORS: shown on the titlebar
#: src/gpm-statistics.c:1269
msgid "Device Profile"
msgstr "Eszköz profilja"

#. TRANSLATORS: shown on the titlebar
#: src/gpm-statistics.c:1271
msgid "Processor Wakeups"
msgstr "Processzorébredések"

#. TRANSLATORS: this is the X axis on the graph
#: src/gpm-statistics.c:1448 src/gpm-statistics.c:1454
#: src/gpm-statistics.c:1460 src/gpm-statistics.c:1466
msgid "Time elapsed"
msgstr "Eltelt idő"

#. TRANSLATORS: this is the Y axis on the graph
#: src/gpm-statistics.c:1450
msgid "Power"
msgstr "Energia"

#. TRANSLATORS: this is the Y axis on the graph for the whole battery device
#. TRANSLATORS: this is the X axis on the graph for the whole battery device
#: src/gpm-statistics.c:1456 src/gpm-statistics.c:1495
#: src/gpm-statistics.c:1501 src/gpm-statistics.c:1507
#: src/gpm-statistics.c:1513
msgid "Cell charge"
msgstr "Cellatöltés"

#. TRANSLATORS: this is the Y axis on the graph
#: src/gpm-statistics.c:1462 src/gpm-statistics.c:1468
msgid "Predicted time"
msgstr "Előrejelzett idő"

#. TRANSLATORS: this is the Y axis on the graph
#: src/gpm-statistics.c:1497 src/gpm-statistics.c:1509
msgid "Correction factor"
msgstr "Javítási tényező"

#. TRANSLATORS: this is the Y axis on the graph
#: src/gpm-statistics.c:1503 src/gpm-statistics.c:1515
msgid "Prediction accuracy"
msgstr "Előrejelzés pontossága"

#. TRANSLATORS: show verbose debugging
#: src/gpm-statistics.c:1646
msgid "Show extra debugging information"
msgstr "Extra hibakeresési információk megjelenítése"

#. TRANSLATORS: show a device by default
#: src/gpm-statistics.c:1649
msgid "Select this device at startup"
msgstr "Ezen eszköz kiválasztása indításkor"

#. TRANSLATORS: the icon for the CPU
#: src/gpm-statistics.c:1893
msgid "Processor"
msgstr "Processzor"

#: src/gpm-statistics.ui:92 src/gpm-statistics.ui:296
msgid "Graph type:"
msgstr "Grafikontípus:"

#: src/gpm-statistics.ui:127
msgid "Data length:"
msgstr "Adathossz:"

#: src/gpm-statistics.ui:186 src/gpm-statistics.ui:348
msgid "There is no data to display."
msgstr "Nincsenek megjeleníthető adatok"

#: src/gpm-statistics.ui:228 src/gpm-statistics.ui:391
msgid "Use smoothed line"
msgstr "Simított vonal használata"

#: src/gpm-statistics.ui:244 src/gpm-statistics.ui:407
msgid "Show data points"
msgstr "Adatpontok megjelenítése"

#: src/gpm-statistics.ui:274
msgid "History"
msgstr "Előzmények"

#: src/gpm-statistics.ui:437
msgid "Statistics"
msgstr "Statisztika"

#: src/gpm-statistics.ui:459
msgid "Processor wakeups per second:"
msgstr "Processzorébredések másodpercenként:"

#: src/gpm-statistics.ui:471
msgid "0"
msgstr "0"

